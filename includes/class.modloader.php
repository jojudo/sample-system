<?php 
	defined('INSTANCE') or die("No Direct access allowed!");
	
	class modloader{
		
		function __construct(){
			$this->boot();
		}
		
		private function boot(){
			$this->db = load_class('db');
			$this->ui  = load_class('ui');
			$this->auth = load_class('auth');
			$this->auth->authenticate();
			
			//$profile = $auth->profile;
			//$current_user = $profile['user_id'];
			
			//$level = $_SESSION['user_level'];
			
			$mod = statics::request("mod");
			$mod = $mod?$mod:"welcome";
			$this->ui->set_module($mod);
			$this->module_process($mod);			
			
		}

		function module_process($mod){
			ob_start();
			if(file_exists(MODPATH.$mod.DS."helper.php")){
				require(MODPATH.$mod.DS."helper.php");
				$module = new moduleHelper;
			}else{
				$this->ui->load_theme_errorfile('Page not found');
			}
			$main_body = ob_get_clean();
			$this->ui->load_theme($main_body);
			
			return ;		
		}
		
		private function authenticate(){
			$this->auth = load_class('authenticate');
			if(!$auth->authenticated){
				echo 'oh no there is a priblem';
				exit();
			}
		}

	}
