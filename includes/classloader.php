<?php

if ( ! function_exists('load_class'))
	{
		function &load_class($class)
		{
			static $_classes = array();
			$prefix = 'class';
			
			// Does the class exist?  If so, we're done...
			if (isset($_classes[$class]))
			{
				return $_classes[$class];
			}

			$name = FALSE;

			
			if (file_exists(CLASSPATH.DS.$prefix.'.'.$class.'.php'))
			{
				$name = $class;

				if (class_exists($name) === FALSE)
				{
					require(CLASSPATH.DS.$prefix.'.'.$class.'.php');
				}
			}else{
					exit("it does not exist");
			}
		

			// Did we find the class?
			if ($name === FALSE)
			{
				// Note: We use exit() rather then show_error() in order to avoid a
				// self-referencing loop with the Excptions class
				exit('Unable to locate the specified class: '.$class.'.php');
			}

			// Keep track of what we just loaded
			is_loaded($class);

			$_classes[$class] = new $name();
			return $_classes[$class];
		}
	}
	
	if ( ! function_exists('is_loaded'))
	{
		function &is_loaded($class = '')
		{
			static $_is_loaded = array();

			if ($class != '')
			{
				$_is_loaded[strtolower($class)] = $class;
			}

			return $_is_loaded;
		}
	}