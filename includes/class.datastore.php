<?php 
	class datastore{ 
		
		function set_config($config_arr){
			$this->config = $config_arr;
		}
		
		function set_profile($profile_arr){
			$this->profile = $profile_arr;
		}
		
		function get_config($subgroup=""){
			if(!$subgroup)
				return array();
			else
				return $this->config[$subgroup];
		}
		
		function get_profile(){
			return $this->profile;
		}
	}
