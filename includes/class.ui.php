<?php
	defined('INSTANCE') or die("No Direct access allowed!");
	class ui{
		protected $stylesheet, $javascript, $datastore, $headerType;
		function __construct(){
			$this->headerType = statics::request("headerType");
			$this->datastore = load_class('datastore');
			$this->load_libraries();
			
		}
		
		function set_module($mod){
			$this->mod = $mod;
		}
		
		function set_stylesheet($link){
			if($this->datastore->get_config('environment')=="development")
				$link.='?temp='.rand(1,9999999);
				
			$this->stylesheet[] = $link;
		}
		
		function set_javascript($link){
			if($this->datastore->get_config('environment')=="development")
				$link.='?temp='.rand(1,9999999);
				
			$this->javascript[] = $link;
		}
		

		function get_theme_head(){
			$args['javascripts'] = $this->javascript;
			$args['stylesheets'] = $this->stylesheet;
			if(isset($this->mod)){
				$args['modPath'] = MOD_ADDR.$this->mod.US;
				$args['mod'] = $this->mod;
			}else{
				$args['modPath'] = '';
				$args['mod'] = '';
			}
			$args['theme_add'] = THEME_ADDR;
			$args['fullPath'] = FULL_ADDR;
			
			return $this->template_headers_auto_setup($args);
		}
		
		function get_theme_navigation(){
			$args['profile'] = $this->datastore->get_profile();
			if(isset($this->mod))
				return $this->load_theme_page('navigation',$args);
			else
				return '';
		}
		function load_theme($body){
			if($this->headerType=="none"){
				print $body;
			}else{
				$headinclude= $this->get_theme_head();
				$topnav = $this->get_theme_navigation();
				include_once(THEME_BASE."index.tpl");				
			}
			return ;
		}
		
		function load_theme_page_solo($filename,$args){
			$tpl = $filename.'.tpl';
			if(file_exists(THEME_BASE.$tpl)){
				extract($args);
				$headinclude= $this->get_theme_head();
				include_once(THEME_BASE.$tpl);
			}else{
				load_theme_errorfile('Page was not found');
			}
			
			return ;
		}
		
		function load_theme_page_noheader($filename,$args){
			$tpl = $filename.'.tpl';
			if(file_exists(THEME_BASE.$tpl)){
				extract($args);
				include_once(THEME_BASE.$tpl);
			}else{
				load_theme_errorfile('Page was not found');
			}
		}
		
		function load_theme_errorfile($msg){
			$args['msg'] = $msg;
			print $buffer = $this->load_theme_page('errorfile',$args);
		}		
		
		function load_theme_page($filename,$args){
			$tpl = $filename.'.tpl';
			ob_start();
			if(file_exists(THEME_BASE.$tpl)){
				extract($args);
				include_once(THEME_BASE.$tpl);
			}else{
				print 'Theme file '.THEME_BASE.$tpl.' is missing ';
			}
			$main_body = ob_get_clean();
			
			return $main_body;
		}
		
		function use_module_flat($filename,$args=array()){
			$tpl = $filename.'.tpl';
			$args['profile'] =  $this->datastore->get_profile();
			$args['module_addr'] = MOD_ADDR.$this->mod.US;
			if(file_exists(MODPATH.$this->mod.DS.'flats'.DS.$tpl)){
				extract($args);
				include(MODPATH.$this->mod.DS.'flats'.DS.$tpl);
			}else{
				print 'Unable to find template '.$tpl;
			}
		}
		
		function load_libraries(){		
			$lib_arr = $this->datastore->get_config('lib');
			if(is_array($lib_arr)){
				foreach($lib_arr['js'] as $js){
					$this->set_javascript(LIB_ADDR.'js'.US.$js);
				}
				
				foreach($lib_arr['css'] as $css){
					$this->set_stylesheet(LIB_ADDR.'css'.US.$css);
				}
			}
		}
		
		function template_headers_auto_setup($args){
			$spacer = "\r\n";
			$str = '<script language="javascript">'.$spacer;
			$str.= 'var fullPath = "'.$args['fullPath'].'"'.$spacer;
			$str.= 'var modPath = "'.$args['modPath'].'"'.$spacer;
			$str.= 'var mod = "'.$args['mod'].'";'.$spacer;
			//if environment
			$str.= 'var environment = "'.$this->datastore->get_config('environment').'";'.$spacer;
			$str.= '</script>'.$spacer;
			
			//javascripts
			if(isset($args['javascripts']) && is_array($args['javascripts'])){
				foreach($args['javascripts'] as $js){
					$str.='<script src="'.$js.'" language="javascript"></script>'.$spacer;
				}
			}
			//stylesheets
			
			if(isset($args['stylesheets']) && is_array($args['stylesheets'])){
				foreach($args['stylesheets'] as $css){					
					$str.='<link rel="stylesheet" type="text/css" media="screen" href="'.$css.'" />'.$spacer;
				}
			}
			
			return $str;
		
		}
		
		function get_header_type(){
			return $this->headerType;
		}
		
	}
?>