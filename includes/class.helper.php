<?php 
	defined('INSTANCE') or die("No Direct access allowed!");
	abstract class helperController{
		function __construct(){
			$action = statics::request("action");
			$action = $action?$action:"main";
			
			$this->db = load_class('db');	
			$this->ui = load_class('ui');
			$this->datastore = load_class('datastore');
			$this->profile = $this->datastore->get_profile();
			
			$this->action = $action;
			
			$this->$action();
		}
		
		abstract protected function main();
				
		function __call($method,$args){
			if($this->ui->get_header_type()=="none"){
				$output_arr['msg'] = "The pages you are looking for cannot be found";
				$output_arr['failure'] = 1;
				echo $this->json_output($output_arr);
			}else{
				echo "<p align='Center'>Unknown $method with ".count($args)." arguments for module <b>".$this->ui->mod."</b></p>";
			}
		}
				
		function json_output($json_arr){
			$args['data_arr'] = $json_arr;
			$args['session'] = 1;
			
			$this->ui->load_theme_page_solo("json",$args);
			
			//return ;
		}
		
		function set_mod_javascript($filename){
			$this->ui->set_javascript(MOD_ADDR.$this->ui->mod.US.'js'.US.$filename.'.js');
		}
		function set_mod_stylesheet($filename){
			$this->ui->set_stylesheet(MOD_ADDR.$this->ui->mod.US.'css'.US.$filename.'.css');
		}
				
	}
?>