<?php 
	defined('INSTANCE') or die("No Direct access allowed!");

	//load configuration file
	if(file_exists(BASE.DS."configuration.php")){
		require BASE.DS."configuration.php";
		//$config = new MConfig();
	}else{
		if(file_exists(BASE.DS."installation".DS."index.php")){
			header('location:'.BASE.DS.'installation'.DS.'index.php');
			exit();	
		}else{
			exit("Installation not found, System Error");
		}
	}

	//load system files
	require SYSPATH.'startupClasses.php';
	require SYSPATH.'processor.php';

?>