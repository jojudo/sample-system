<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<head>
	<title>Title</title>
	<?php echo $headinclude;?>
	<!-- custom headers here  -->
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo THEME_ADDR;?>css/main.css" />
</head>
<body topmargin="0">
	<div id="container">
		<div id="banner">
			<a href="index.php"><img src="<?php echo THEME_ADDR;?>images/logo.jpg" border="0"></a>
		</div>
		<div id="top-nav">
			<?php echo $topnav;?>
		</div>
		<div id="inside-content">
				<?php echo $body;?>
		</div>
		<div id="footer">
			Copyright &copy; <?=date("Y");?> knight. All rights reserved. 
		</div>
	</div>

</body>
</html>