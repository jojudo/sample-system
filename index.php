<?php
	session_start();
	define('INSTANCE',1);
	
	//define environment
	define('ENVIRONMENT', 'development');	
	
	//setup static refences
	define('DS',DIRECTORY_SEPARATOR);
	define('US','/');
	
	//let us define the location of main processors!
	$system_path = realpath('system').DS;
	define('SYSPATH', str_replace("\\", DS, $system_path));
	
	//define this file
	define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
	
	//define the base of this file
	define('BASE',dirname(__FILE__));
	
	//let us define external libraries path
	define('LIBPATH', BASE.DS.'libraries'.DS);

	//let us define the internal classes path
	define('CLASSPATH', BASE.DS.'includes'.DS);
	
	//let us define the internal classes path
	define('MODPATH', BASE.DS.'modules'.DS);
	
	//let us define the Full URL versions of the folders
	$addr_arr = explode(US,$_SERVER['SCRIPT_NAME']);
	array_pop($addr_arr);

	$current_folder = implode(US,$addr_arr);	
	$current_folder = LTRIM($current_folder,DS);
	$server_name = rtrim($_SERVER['SERVER_NAME']);
	
	//full URL Path
	define('FULL_ADDR',"http://".$server_name.$current_folder.US);
	//full Module URL Path
	define('MOD_ADDR',"http://".$server_name.$current_folder.US.'modules'.US);
	
	//full URL Lib Path
	define('LIB_ADDR',"http://".$server_name.$current_folder.US.'libraries'.US);
	
	//load the mother of all boards
	require_once SYSPATH.'motherboard.php';

?>

