<?php 
//bring down the site
$config['offline'] = 0;
$config['offline_msg'] = 'Im sorry site is under construction';
$config['session_timeout'] = 1800; //30 minutes per refresh
$config['strike_limit'] = 5; //5 wrong password
$config['strike_wait'] = 300; //5 minutes wait for fresh 5 strike bat

$config['timezone'] = 'Pacific/Samoa';
$config['environment'] = 'development';
$config['theme'] = 'default';

//database settings
$config['db']['host'] = 'localhost';
$config['db']['database'] = 'dbname';
$config['db']['driver'] = 'mysql';
$config['db']['username'] = 'root';
$config['db']['password'] = '';

//li braries
//for javascripts
$config['lib']['js'] = array('jquery/jquery-1.10.2.js','jquery/jquery-ui.min.js','framework/main.js');

//for css
$config['lib']['css'] = array('jquery/jquery-ui.min.css');

//extras
$config['data']['limit'] = 20;


